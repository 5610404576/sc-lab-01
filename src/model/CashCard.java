package model;


public class CashCard
{  
   private double balance;

 
   public CashCard()
   {   
      balance = 0;
   }

    public CashCard(double initialBalance)
   {   
      balance = initialBalance;
   }
 
   public void addmoney(double amount)
   {  
      double newBalance = balance + amount;
      balance = newBalance;
   }


   public void paymoney(double amount)
   {   
      double newBalance = balance - amount;
      balance = newBalance;
   }

  
   public double getBalance()
   {   
      return balance;
   }
}
