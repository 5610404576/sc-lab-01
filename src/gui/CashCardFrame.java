package gui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.CashCard;


public class CashCardFrame extends JFrame
{    
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 150;

   private JLabel AddLabel, PayLabel;
   private JTextField AddField , PayField;
   private JButton button1 , button2;
   private JLabel resultLabel;
   private JPanel panel1, panel2, panel3;
   private CashCard account;
    
   public CashCardFrame()
   {  
      account = new CashCard();
      resultLabel = new JLabel("balance: " + account.getBalance());

      createTextField();
      createButton();
      createPanel();

      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }

   private void createTextField()
   {
      AddLabel = new JLabel("Add Money: ");
      PayLabel = new JLabel("Pay Money: ");

      AddField = new JTextField(10);
      AddField.setText("");
      
      PayField = new JTextField(10);
      PayField.setText("");
   }
   
   private void createButton()
   {
      button1 = new JButton("Add");
      
      class AddInterestListener implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            double amount = Double.parseDouble(AddField.getText());
            account.addmoney(amount);
            AddField.setText("");
            resultLabel.setText("balance: " + account.getBalance());
         }            
      }
      
      ActionListener listener = new AddInterestListener();
      button1.addActionListener(listener);
      
      
      button2 = new JButton("Pay");
      
      class AddInterestListener2 implements ActionListener
      {
         public void actionPerformed(ActionEvent event)
         {
            double amount = Double.parseDouble(PayField.getText());
            account.paymoney(amount);
            PayField.setText("");
            resultLabel.setText("balance: " + account.getBalance());
         }            
      }
      
      ActionListener listener2 = new AddInterestListener2();
      button2.addActionListener(listener2);
   }
   
   private void createPanel()
   {
	  setLayout(new GridLayout(3,0));
      panel1 = new JPanel();
      panel1.add(AddLabel);
      panel1.add(AddField);
      panel1.add(button1);
      panel1.add(resultLabel);      
      add(panel1);
      panel2 = new JPanel();
	  panel2.add(PayLabel);
	  panel2.add(PayField);
	  panel2.add(button2);
	  add(panel2);
	  panel3 = new JPanel();
      panel3.add(resultLabel);   
      add(panel3);
   } 
}
